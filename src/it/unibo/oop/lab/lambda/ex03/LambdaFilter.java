package it.unibo.oop.lab.lambda.ex03;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.Toolkit;
import java.util.Arrays;
import java.util.function.Function;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 * Modify this small program adding new filters.
 * 
 * 1) Convert to lowercase
 * 
 * 2) Count the number of chars
 * 
 * 3) Count the number of lines
 * 
 * 4) List all the words in alphabetical order
 * 
 * 5) Write the count for each word, e.g. "word word pippo" should output "pippo
 * -> 1 word -> 2
 *
 */
public final class LambdaFilter extends JFrame {

    private static final long serialVersionUID = 1760990730218643730L;

    private enum Command {
        IDENTITY("No modifications", s -> s), TOLOWERCASE("Convert to lowercase",
                String::toLowerCase), 
        COUNTNUMBEROFCHARS("Count the number of chars",
                        s -> Integer.toString(s.length())), 
        /*LINES("Count lines", s -> Long.toString(s.chars().filter(e -> e == '\n').count() + 1)),
        WORDS("Sort words in alphabetical order", s ->
            Arrays.stream(s.split(ANY_NON_WORD))
                .sorted()
                .collect(Collectors.joining("\n"))),
        WORDCOUNT("Count words", s ->
            Arrays.stream(s.split(ANY_NON_WORD))
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet().stream()
                .map(e -> e.getKey() + " -> " + e.getValue())
                .collect(Collectors.joining("\n"))
        )*/
        COUNTNUMBEROFLINES("Count the number of lines",
                s -> Integer
                .toString(s.split(System.getProperty("line.separator")).length)), 
        WORDINORDER("List all the words in alphabetical order", 
                s -> Arrays.toString(Arrays.asList(s.split("\\s+"))
                        .stream().sorted().toArray())),
        COUNTOFEACHWORD("Write the count for each word", 
                s -> {
                final StringBuilder sb = new StringBuilder();
                Arrays.asList(s.split("\\s+")).stream()
                .collect(groupingBy(e -> e, counting())).forEach((k, v) -> {
                    sb.append(k);
                    sb.append(" -> ");
                    sb.append(v);
                    sb.append('\n');
                });
                return sb.toString();
                });
        private final String commandName;
        private final Function<String, String> fun;

        Command(final String name, final Function<String, String> process) {
            commandName = name;
            fun = process;
        }

        @Override
        public String toString() {
            return commandName;
        }

        public String translate(final String s) {
            return fun.apply(s);
        }

    }

    private LambdaFilter() {
        super("Lambda filter GUI");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        final JPanel panel1 = new JPanel();
        final LayoutManager layout = new BorderLayout();
        panel1.setLayout(layout);

        final JComboBox<Command> combo = new JComboBox<>(Command.values());
        panel1.add(combo, BorderLayout.NORTH);

        final JPanel centralPanel = new JPanel(new GridLayout(1, 2));
        final JTextArea left = new JTextArea();
        left.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        final JTextArea right = new JTextArea();
        right.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        right.setEditable(false);
        centralPanel.add(left);
        centralPanel.add(right);

        panel1.add(centralPanel, BorderLayout.CENTER);

        final JButton apply = new JButton("Apply");
        apply.addActionListener((ev) -> right.setText(((Command) combo.getSelectedItem()).translate(left.getText())));
        panel1.add(apply, BorderLayout.SOUTH);

        setContentPane(panel1);

        final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        final int sw = (int) screen.getWidth();
        final int sh = (int) screen.getHeight();
        setSize(sw / 4, sh / 4);

        setLocationByPlatform(true);
    }

    /**
     * @param a
     *            unused
     */
    public static void main(final String... a) {
        final LambdaFilter gui = new LambdaFilter();
        gui.setVisible(true);
    }

}
