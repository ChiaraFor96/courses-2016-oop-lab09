package it.unibo.oop.lab.lambda.ex02;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 */
public class MusicGroupImpl implements MusicGroup {

    private final Map<String, Integer> albums = new HashMap<>();
    private final Set<Song> songs = new HashSet<>();

    @Override
    public void addAlbum(final String albumName, final int year) {
        this.albums.put(albumName, year);
    }

    @Override
    public void addSong(final String songName, final Optional<String> albumName, final double duration) {
        if (albumName.isPresent() && !this.albums.containsKey(albumName.get())) {
            throw new IllegalArgumentException("invalid album name");
        }
        this.songs.add(new MusicGroupImpl.Song(songName, albumName, duration));
    }

    @Override
    public Stream<String> orderedSongNames() {
        return songs.stream().map(Song::getSongName).sorted();
    }

    @Override 
    public Stream<String> albumNames() {
        return this.albums.keySet().stream();
        /*final List<String> list = new ArrayList<>();
        albums.forEach((name, year) -> {
            list.add(name);
        });
        return list.stream();*/
    }

    @Override
    public Stream<String> albumInYear(final int year) {
        /*
        final List<String> list = new ArrayList<>();
        albums.forEach((s, y) -> {
            if (y == year) {
                list.add(s);
            }
        });
        return list.stream();*/
        return this.albums.entrySet().stream()
                .filter(e -> e.getValue() == year).map(e -> e.getKey());
    }

    @Override
    public int countSongs(final String albumName) {
        return (int) songs.stream().filter(s -> s.getAlbumName().filter(x -> x.equals(albumName)).isPresent()).count();
    }

    @Override
    public int countSongsInNoAlbum() {
        return (int) songs.stream().map(Song::getAlbumName).filter(s -> s.equals(Optional.empty())).count();
    }

    @Override
    public OptionalDouble averageDurationOfSongs(final String albumName) {
        return songs.stream().filter(s -> s.getAlbumName().filter(x -> x.equals(albumName)).isPresent())
                .mapToDouble(Song::getDuration).average();
    }

    @Override
    public Optional<String> longestSong() {
        /*
         * return this.songs.stream()
                .collect(Collectors.maxBy((a, b) -> 
                    Double.compare(a.getDuration(), b.getDuration())))
                .map(Song::getSongName);
         */
        return Optional.of(songs.stream()
                .filter(s -> s.getDuration() == songs.stream().mapToDouble(Song::getDuration).max().getAsDouble())
                .findFirst().get().getSongName());
    }

    @Override 
    public Optional<String> longestAlbum() {
        /*int max = 0;
        Optional<String> maxAlbum = Optional.empty();
        int duration;
        for (final String album : albums.keySet()) {
            duration = 0;
            for (final Song s : songs) {
                if (s.getAlbumName().equals(Optional.of(album))) {
                    duration += s.getDuration();
                }
            }
            if (duration >= max) {
                max = duration;
                maxAlbum = Optional.of(album);
            }
        }
        return maxAlbum;*/
        return this.songs.stream().filter(a -> a.getAlbumName().isPresent())
                .collect(Collectors.groupingBy(Song::getAlbumName, 
                        Collectors.summingDouble(Song::getDuration)))
                .entrySet().stream()
                .collect(Collectors.maxBy((e1, e2) -> Double.compare(e1.getValue(), 
                        e2.getValue()))).get().getKey();
    }

    private static final class Song {

        private final String songName;
        private final Optional<String> albumName;
        private final double duration;
        private int hash;

        private Song(final String name, final Optional<String> album, final double len) {
            super();
            this.songName = name;
            this.albumName = album;
            this.duration = len;
        }

        public String getSongName() {
            return songName;
        }

        public Optional<String> getAlbumName() {
            return albumName;
        }

        public double getDuration() {
            return duration;
        }

        @Override
        public int hashCode() {
            if (hash == 0) {
                hash = songName.hashCode() ^ albumName.hashCode() ^ Double.hashCode(duration);
            }
            return hash;
        }

        @Override
        public boolean equals(final Object obj) {
            if (obj instanceof Song) {
                final Song other = (Song) obj;
                return albumName.equals(other.albumName) && songName.equals(other.songName)
                        && duration == other.duration;
            }
            return false;
        }

        @Override
        public String toString() {
            return "Song [songName=" + songName + ", albumName=" + albumName + ", duration=" + duration + "]";
        }

    }

}
